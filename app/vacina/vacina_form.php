<?php
$form = new GForm();

//<editor-fold desc="Header">
$title = '<span class="acaoTitulo"></span>';
$tools = '<a id="f__btn_voltar"><i class="fa fa-arrow-left font-blue-steel"></i> <span class="hidden-phone font-blue-steel bold uppercase">Voltar</span></a>';
$htmlForm .= getWidgetHeader($title, $tools);
//</editor-fold>
//<editor-fold desc="Formulário">
$htmlForm .= $form->open('form', 'form-vertical form');
$htmlForm .= $form->addInput('hidden', 'acao', false, array('value' => 'ins', 'class' => 'acao'), false, false, false);
$htmlForm .= $form->addInput('hidden', 'anivac_int_codigo', false, array('value' => ''), false, false, false);
$htmlForm .= $form->addSelect('ani_int_codigo', array(), '', 'Animal*', array('validate' => 'required'), false, false, true, '', 'Selecione...');
$htmlForm .= $form->addSelect('vac_int_codigo', array(), '', 'Vacina*', array('validate' => 'required'), false, false, true, '', 'Selecione...');
$htmlForm .= $form->addInput('text', 'anv_dat_programacao', 'Data Programada*', array('maxlength' => '50', 'validate' => 'required'));
$htmlForm .= $form->addInput('text', 'anv_dti_aplicada', 'Data Aplicada*', array('maxlength' => '50', 'validate' => 'required'));
$htmlForm .= $form->addSelect('usu_int_codigo', array(), '', 'Usuário*', array('validate' => 'required'), false, false, true, '', 'Selecione...');


$htmlForm .= '<div class="form-actions">';
$htmlForm .= getBotoesAcao(true);
$htmlForm .= '</div>';
$htmlForm .= $form->close();
//</editor-fold>
$htmlForm .= getWidgetFooter();

echo $htmlForm;
?>
<script>
    $(function() {
        $("#anv_dat_programacao").mask("99/99/9999");
        $("#anv_dti_aplicada").mask("99/99/9999");

        $('#form').submit(function() {
            var anivac_int_codigo = $('#anivac_int_codigo').val();
            $('#p__selecionado').val();
            if ($('#form').gValidate()) {
                var method = ($('#acao').val() == 'ins') ? 'POST' : 'PUT';
                var endpoint = ($('#acao').val() == 'ins') ? URL_API + 'vacina' : URL_API + 'vacina/' + anivac_int_codigo;
                $.gAjax.exec(method, endpoint, $('#form').serializeArray(), false, function(json) {
                    if (json.status) {
                        showList(true);
                    }
                });
            }
            return false;
        });

        $.gAjax.exec('GET', URL_API + 'vacina/animal/all', false, false, function(json) {
            $.each(json, function(index, value){
                $("#ani_int_codigo").append('<option value="'+ value.codigo +'">'+ value.nome +'</option>')
            })            
        });

        $.gAjax.exec('GET', URL_API + 'vacina/vacina/all', false, false, function(json) {
            $.each(json, function(index, value){
                $("#vac_int_codigo").append('<option value="'+ value.codigo +'">'+ value.nome +'</option>')
            })            
        });

        $.gAjax.exec('GET', URL_API + 'vacina/usuario/all', false, false, function(json) {
            $.each(json, function(index, value){
                $("#usu_int_codigo").append('<option value="'+ value.codigo +'">'+ value.nome +'</option>')
            })            
        });                               

        $('#f__btn_cancelar, #f__btn_voltar').click(function() {
            showList();
            return false;
        });

        $('#f__btn_excluir').click(function() {
            var ani_int_codigo = $('#ani_int_codigo').val();

            $.gDisplay.showYN("Quer realmente deletar o item selecionado?", function() {
                $.gAjax.exec('DELETE', URL_API + 'usuarios/' + ani_int_codigo, false, false, function(json) {
                    if (json.status) {
                        showList(true);
                    }
                });
            });
        });
    });
</script>